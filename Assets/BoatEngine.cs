﻿ 
using UnityEngine;
using System.Collections;

public class BoatEngine : MonoBehaviour 
{
	//Drags
	public Transform waterJetTransform;

	//How fast should the engine accelerate?
	public float powerFactor;

	//What's the boat's maximum engine power?
	public float maxPower;

	//The boat's current engine power is public for debugging
	public float currentJetPower;

	private float thrustFromWaterJet = 0f;

	private Rigidbody boatRB;

	private float WaterJetRotation_Y = 0f;

	BoatController boatController;

	void Start() 
	{
		boatRB = GetComponent<Rigidbody>();

		boatController = GetComponent<BoatController>();
	}


	void Update() 
	{
		UserInput();
	}

	void FixedUpdate()
	{
		UpdateWaterJet();
	}

	void UserInput()
	{
		//Forward / reverse
		if (Input.GetKey(KeyCode.W))
		{
			//if (boatController.CurrentSpeed < 50f && currentJetPower < maxPower)
			//{
				currentJetPower += 1f * powerFactor;
			Debug.Log("pressed W");
			//}
		}
		else
		{
			currentJetPower = 0f;
		}

		//Steer left
		if (Input.GetKey(KeyCode.A))
		{
			WaterJetRotation_Y = waterJetTransform.localEulerAngles.y + 2f;

			if (WaterJetRotation_Y > 30f && WaterJetRotation_Y < 270f)
			{
				WaterJetRotation_Y = 30f;
			}

			Vector3 newRotation = new Vector3(0f, WaterJetRotation_Y, 0f);

			waterJetTransform.localEulerAngles = newRotation;
		}
		//Steer right
		else if (Input.GetKey(KeyCode.D))
		{
			WaterJetRotation_Y = waterJetTransform.localEulerAngles.y - 2f;

			if (WaterJetRotation_Y < 330f && WaterJetRotation_Y > 90f)
			{
				WaterJetRotation_Y = 330f;
			}

			Vector3 newRotation = new Vector3(0f, WaterJetRotation_Y, 0f);

			waterJetTransform.localEulerAngles = newRotation;
		}
	}

	void UpdateWaterJet()
	{
		//Debug.Log(boatController.CurrentSpeed);

		Vector3 forceToAdd = -waterJetTransform.forward * currentJetPower;

		//Only add the force if the engine is below sea level
		float waveYPos = WaterController.current.GetWaveYPos(waterJetTransform.position, Time.time);

		if (waterJetTransform.position.y < waveYPos)
		{
			boatRB.AddForceAtPosition(forceToAdd, waterJetTransform.position);
		}
		else
		{
			boatRB.AddForceAtPosition(Vector3.zero, waterJetTransform.position);
		}
	}
}	
/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatEngine : MonoBehaviour {

	public float velocity = 3.0f;

//	public float speedH = 2.0f;
//	public float speedV = 2.0f;

//	private float yaw = 0.0f;
//	private float pitch = 0.0f;

//	public int speed;

	private CharacterController cc;

	void Start ()
	{
		cc = GetComponent<CharacterController> ();	
	}

	void Update () 
	{   
	//	yaw += speedH * Input.GetAxis("Mouse X");
	//	pitch -= speedV * Input.GetAxis("Mouse Y");
	//	transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);

		if (Input.GetKeyDown (KeyCode.W))
		{	Debug.Log("w key was pressed.");
			cc.SimpleMove (velocity); 
		}

		if (Input.GetKeyDown(KeyCode.S))
		{	Debug.Log("s key was pressed.");
			cc.SimpleMove (velocity); 
		}

		if (Input.GetKeyDown(KeyCode.A))
		{	Debug.Log("a key was pressed.");
			cc.SimpleMove (velocity); 
		}

		if (Input.GetKeyDown(KeyCode.D))
		{	Debug.Log("d key was pressed.");
			cc.SimpleMove (velocity); 
		}
	  
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		GetComponent<Rigidbody> ().velocity = movement*speed;

		GetComponent<Rigidbody> ().position = new Vector3
			(
				Mathf.Clamp (GetComponent<Rigidbody> ().position.x, boundary.xMin, boundary.xMax), 
				0.0f, 
				Mathf.Clamp (GetComponent<Rigidbody> ().position.z, boundary.zMin, boundary.zMax)

			);

		GetComponent<Rigidbody> ().rotation=Quaternion.Euler(0.0f, 0.0f, (GetComponent<Rigidbody> ().velocity.x) * -tilt);

		
	}
}
*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary 
{
	public float xMin, xMax, zMin, zMax;
}
public class PlayerController : MonoBehaviour 
{
	public float speed ;
	public float tilt;
	public Boundary boundary;

	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;

	private float nextFire;

	void Update ()
	{
		if (Input.GetButton("Fire1") && Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			GetComponent<AudioSource>().Play();		}
	}


	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		GetComponent<Rigidbody> ().velocity = movement*speed;

		GetComponent<Rigidbody> ().position = new Vector3
			(
				Mathf.Clamp (GetComponent<Rigidbody> ().position.x, boundary.xMin, boundary.xMax), 
				0.0f, 
				Mathf.Clamp (GetComponent<Rigidbody> ().position.z, boundary.zMin, boundary.zMax)

			);

		GetComponent<Rigidbody> ().rotation=Quaternion.Euler(0.0f, 0.0f, (GetComponent<Rigidbody> ().velocity.x) * -tilt);


	}


}
*/